package main;

import modelo.Agenda;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.ControladorVistaGeneralPersona;
import presentacion.vista.VistaGeneralPersona;


public class Main 
{
	
	// PRINCIPAL
	public static void main(String[] args) 
	{
		
		VistaGeneralPersona vista = new VistaGeneralPersona();
		Agenda modelo = new Agenda(new DAOSQLFactory());
		ControladorVistaGeneralPersona controlador = new ControladorVistaGeneralPersona(vista, modelo);
		controlador.inicializar();
		
	}
}
