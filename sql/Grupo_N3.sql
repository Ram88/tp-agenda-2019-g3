drop database agenda;

CREATE DATABASE `agenda`;

USE agenda;

CREATE TABLE `tipoDeContacto`
(
  `idTipoDeContacto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`idTipoDeContacto`)
);

CREATE TABLE `localidad`
(
  `idLocalidad` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`idLocalidad`)
);

CREATE TABLE `domicilio`
(
  `idDomicilio` int(11) NOT NULL AUTO_INCREMENT,
  `calle` varchar(45) NOT NULL,
  `altura` varchar(11) NOT NULL,
  `piso` varchar(45) NOT NULL,
  `departamento` varchar(45) NOT NULL,
  `idLocalidad` int(11) NOT NULL,
  PRIMARY KEY (`idDomicilio`)
  -- FOREIGN KEY (idPersona) REFERENCES persona(idPersona) 
);

CREATE TABLE `personas`
(
  `idPersona` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `idDomicilio` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `fechaCumple` varchar(11) NOT NULL,
  `idTipoDeContacto` int(11) NOT NULL,
  PRIMARY KEY (`idPersona`),
  FOREIGN KEY (idTipoDeContacto) REFERENCES tipoDeContacto(idTipoDeContacto) 
);

-- Cargar Datos
INSERT INTO tipoDeContacto (nombre) VALUES ('Amigo');
INSERT INTO tipoDeContacto (nombre) VALUES ('Familia');
INSERT INTO tipoDeContacto (nombre) VALUES ('Trabajo');

INSERT INTO localidad (nombre) VALUES ('San Fernando');
INSERT INTO localidad (nombre) VALUES ('San Isidro');
INSERT INTO localidad (nombre) VALUES ('Moreno');


-- INSERT INTO domicilio (idDomicilio, calle, altura, piso, departamento, idLocalidad) VALUES (0, 'Portugal', '5615', '5', 'A', 1);

-- INSERT INTO personas ( nombre, telefono ,idDomicilio ,email ,fechaCumple, idTipoDeContacto )  VALUES ('Juan Perez', '47140433', 0, 'juanperez@mail.com','12/02/2003',1);


-- select * from localidad;
-- select * from personas;
-- select * from domicilio;

