package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import dto.LocalidadDTO;
import modelo.Agenda;
import presentacion.vista.VentanaAgregarLocalidad;

public class ControladorAgregarLocalidad implements ActionListener {
	
	//INSTANCIAS
	private VentanaAgregarLocalidad ventanaAgregarLocalidad;
	private Agenda agenda;
	private ControladorVistaDeLocalidades controlLocalidad;

	// CONSTRUCTOR
	public ControladorAgregarLocalidad(Agenda agenda,
			VentanaAgregarLocalidad ventana,
			ControladorVistaDeLocalidades control) {
		this.ventanaAgregarLocalidad = ventana;
		this.agenda = agenda;
		this.controlLocalidad = control;
		this.ventanaAgregarLocalidad.getBtnAgregarLocalidad()
				.addActionListener(this);
	}

	// METODOS**************************
	public void initialize() {
		this.ventanaAgregarLocalidad.show();
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.ventanaAgregarLocalidad.getBtnAgregarLocalidad()) {
			LocalidadDTO localidad = new LocalidadDTO(0,
			this.ventanaAgregarLocalidad.getTextLocalidad().getText());
			this.agenda.agregarLocalidad(localidad);
			
			this.ventanaAgregarLocalidad.show();
			this.controlLocalidad.initialize(); // SE ACTUALIZA VENTANA ANTES DE CERRAR.
			this.ventanaAgregarLocalidad.close();
		}
	}

}
