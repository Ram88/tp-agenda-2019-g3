package persistencia.dao.mysql;

import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.DomicilioDAO;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.TipoDeContactoDAO;

// CLASE DE INTERFAZ PARA CREAR LA PERSISTENCIA DE OBJETOS DAO EN BDD
public class DAOSQLFactory implements DAOAbstractFactory 
{
	/** METODO QUE RETORNA LA PERSISTENCIA EN BDD DE PERSONA DAO*/
	public PersonaDAO createPersonaDAO() 
	{
		return new PersonaDAOSQL();
	}

	public DomicilioDAO createDomicilioDAO() 
	{
		return new DomicilioDAOSQL();
	}

	/** METODO QUE RETORNA LA PERSISTENCIA EN BDD DE TIPO DE CONTACTO DAO*/
	public TipoDeContactoDAO createTipoDeContactoDAO() 
	{
		return new TipoDeContactoDAOSQL();
	}

	public LocalidadDAO createLocalidadDAO() 
	{
		return new LocalidadDAOSQL();
	}
	
}
