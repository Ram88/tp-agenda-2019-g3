package presentacion.vista;

import javax.swing.JFrame;
import javax.swing.JTextField;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.SwingConstants;

public class VentanaAgregarLocalidad {

	private JFrame frame;
	private JTextField textLocalidad;
	private JButton btnAgregarLocalidad;

	//	CONSTRUCTOR
	public VentanaAgregarLocalidad() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Agregar Localidad");
		frame.setSize(300, 150);
		frame.getContentPane().setBackground(new Color(153, 204, 204));
	    frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textLocalidad = new JTextField();
		textLocalidad.setHorizontalAlignment(SwingConstants.CENTER);
		textLocalidad.setBounds(20, 22, 241, 20);
		frame.getContentPane().add(textLocalidad);
		textLocalidad.setColumns(10);
		
		btnAgregarLocalidad = new JButton("Agregar");
		btnAgregarLocalidad.setBounds(20, 65, 241, 30);
		btnAgregarLocalidad.setForeground(Color.BLACK);
		btnAgregarLocalidad.setBackground(new Color(0, 51, 255));
		frame.getContentPane().add(btnAgregarLocalidad);
	}

	public JTextField getTextLocalidad() {
		return textLocalidad;
	}

	public JButton getBtnAgregarLocalidad() {
		return btnAgregarLocalidad;
	}

	public void show() {
		frame.setVisible(true);
	}
	
    public void close()
	{
    	frame.dispose();
	}
	
}
