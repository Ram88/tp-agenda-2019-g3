package persistencia.dao.interfaz;

import java.util.List;

import dto.PersonaDTO;

// CLASE INTERFAZ DE OBJETO PERSONA DAO
public interface PersonaDAO 
{
	public boolean insert(PersonaDTO persona);

	public boolean delete(PersonaDTO persona_a_eliminar);

	public boolean update(PersonaDTO persona);

	public List<PersonaDTO> readAll();
}
