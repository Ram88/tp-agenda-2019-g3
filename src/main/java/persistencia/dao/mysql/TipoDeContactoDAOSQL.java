package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.TipoDeContactoDAO;
import dto.TipoDeContactoDTO;

//CLASE QUE IMPLEMENTA LA INTERFAZ DE TIPO DE CONTACTO DAO PARA LA PERSISTENCIA EN BDD.
public class TipoDeContactoDAOSQL implements TipoDeContactoDAO
{
	private static final String insert = "INSERT INTO tipoDeContacto(idTipoDeContacto, nombre) VALUES(?, ?)";
	private static final String update = "UPDATE tipoDeContacto Set nombre = ? Where idTipoDeContacto = ?";
	private static final String delete = "DELETE FROM tipoDeContacto WHERE idTipoDeContacto = ? ";
	private static final String readall = "SELECT * FROM tipoDeContacto";
	private static final String traerPorNombre = "SELECT idTipoDeContacto FROM tipoDeContacto Where nombre = ?";
	private static final String readForId = "SELECT * FROM tipoDeContacto WHERE idTipoDeContacto = ? ";

	public boolean insert(TipoDeContactoDTO tipoDeContacto) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setInt(1, tipoDeContacto.getIdTipoDeContacto());
			statement.setString(2, tipoDeContacto.getNombre());
			if(statement.executeUpdate() > 0) //Si se ejecut� devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean update(TipoDeContactoDTO tipoDeContacto) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setString(1, tipoDeContacto.getNombre());
			statement.setInt(2, tipoDeContacto.getIdTipoDeContacto());
			if(statement.executeUpdate() > 0) //Si se ejecut� devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean delete(TipoDeContactoDTO tipoDeContacto) {
		PreparedStatement statement;
		int chequeoUpdate = 0;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(delete);
			statement.setString(1, Integer.toString(tipoDeContacto.getIdTipoDeContacto()));
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) //Si se ejecutó devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	public List<TipoDeContactoDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<TipoDeContactoDTO> tiposDeContactos = new ArrayList<TipoDeContactoDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				tiposDeContactos.add(new TipoDeContactoDTO(resultSet.getInt("idTipoDeContacto"), resultSet.getString("nombre")));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return tiposDeContactos;
	}

	public int traerporNombre(String nombre) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(traerPorNombre);
			statement.setString(1, nombre);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				return resultSet.getInt("idTipoDeContacto");
			}
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return 0;
	}

	public TipoDeContactoDTO readForId(int idTipoDeContacto) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readForId);
			statement.setInt(1, idTipoDeContacto);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				return new TipoDeContactoDTO(resultSet.getInt("idTipoDeContacto"), resultSet.getString("nombre"));
			}	
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return null;
	}


}
