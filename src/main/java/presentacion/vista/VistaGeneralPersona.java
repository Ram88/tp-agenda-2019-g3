package presentacion.vista;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import persistencia.conexion.Conexion;
import dto.PersonaDTO;
import java.awt.Color;

// VENTANA PRINCIPAL DE LISTA DE CONTACTOS.
public class VistaGeneralPersona
{
	private JFrame frame;
	private JTable tablaPersonas;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JButton btnReporte;
	private JButton btnAbmLocalidad;
	private JButton btnAbmTipoCon;

	private DefaultTableModel modelPersonas;
	private  String[] nombreColumnas = {"Nombre y Ap.","Telefono","Email","Cumplea�os","Localidad",
										"Calle","Altura","Piso","Dep.","Tipo Contacto"};

	// CONSTRUCTOR
	public VistaGeneralPersona() 
	{
		super();
		initialize();
	}

	// METODO DE INICIALIZACION DE VENTANA.
	private void initialize() 
	{
		frame = new JFrame();
		frame.setBounds(10, 10, 950, 369);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);//********* PARA CENTRAR EN LA PANTALLA.
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 934, 330);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setBounds(10, 11, 914, 182);
		panel.add(spPersonas);
		
		modelPersonas = new DefaultTableModel(null,nombreColumnas);
		tablaPersonas = new JTable(modelPersonas);
		
		// LLENADO DE LAS COLUMNAS
		tablaPersonas.getColumnModel().getColumn(0).setPreferredWidth(120);
		tablaPersonas.getColumnModel().getColumn(0).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(70);
		tablaPersonas.getColumnModel().getColumn(1).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(2).setPreferredWidth(50);
		tablaPersonas.getColumnModel().getColumn(2).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(3).setPreferredWidth(80);
		tablaPersonas.getColumnModel().getColumn(3).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(4).setPreferredWidth(70);
		tablaPersonas.getColumnModel().getColumn(4).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(5).setPreferredWidth(50);
		tablaPersonas.getColumnModel().getColumn(5).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(6).setPreferredWidth(50);
		tablaPersonas.getColumnModel().getColumn(6).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(7).setPreferredWidth(50);
		tablaPersonas.getColumnModel().getColumn(7).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(8).setPreferredWidth(80);
		tablaPersonas.getColumnModel().getColumn(8).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(9).setPreferredWidth(80);
		tablaPersonas.getColumnModel().getColumn(9).setResizable(false);
		
		spPersonas.setViewportView(tablaPersonas);
		
		btnAgregar = new JButton("Agregar Contacto");
		btnAgregar.setBackground(new Color(51, 204, 153));
		btnAgregar.setBounds(43, 228, 151, 23);
		panel.add(btnAgregar);
		
		btnEditar = new JButton("Editar Contacto");
		btnEditar.setBackground(new Color(204, 204, 51));
		btnEditar.setBounds(272, 228, 142, 23);
		panel.add(btnEditar);
		
		btnBorrar = new JButton("Eliminar Contacto");
		btnBorrar.setBackground(new Color(255, 99, 71));
		btnBorrar.setBounds(509, 228, 142, 23);
		panel.add(btnBorrar);
		
		btnReporte = new JButton("Reporte");
		btnReporte.setBackground(new Color(224, 255, 255));
		btnReporte.setBounds(727, 228, 115, 23);
		panel.add(btnReporte);
		
		btnAbmLocalidad = new JButton("ABM Localidad");
		btnAbmLocalidad.setBackground(new Color(147, 112, 219));
		btnAbmLocalidad.setBounds(233, 284, 203, 23);
		panel.add(btnAbmLocalidad);
		
		btnAbmTipoCon = new JButton("ABM Tipo De Contacto");
		btnAbmTipoCon.setBackground(new Color(147, 112, 219));
		btnAbmTipoCon.setBounds(480, 284, 203, 23);
		panel.add(btnAbmTipoCon);
	}
	
	public void show()
	{
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "�Estas seguro que quieres salir de la Agenda?", 
		             		"Confirmacion", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        	Conexion.getConexion().cerrarConexion();
		           System.exit(0);
		        }
		    }
		});
		this.frame.setVisible(true);
	}

	// **** BOTONES ****
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}

	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getBtnReporte() 
	{
		return btnReporte;
	}
	
	public DefaultTableModel getModelPersonas() 
	{
		return modelPersonas;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaPersonas;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}
	
	public JButton getBtnEditar() {
		return btnEditar;
	}

	public JButton getBtnAbmLocalidad() {
		return btnAbmLocalidad;
	}

	public JButton getBtnAbmTipoCon() {
		return btnAbmTipoCon;
	}

	// METODO PARA LLENADO DE TABLA.
	public void llenarTabla(List<PersonaDTO> personasEnTabla) {
		this.getModelPersonas().setRowCount(0); //Para vaciar la tabla
		this.getModelPersonas().setColumnCount(0);
		this.getModelPersonas().setColumnIdentifiers(this.getNombreColumnas());

		for (PersonaDTO p : personasEnTabla)
		{
			String nombre = p.getNombre();
			String tel = p.getTelefono();
			String mail = p.getEmail();
			String cumple = p.getFechaNacimiento();
			String loc = p.getDomicilio().getLocalidad().getNombre();
			String calle = p.getDomicilio().getCalle();
			String alt = p.getDomicilio().getAltura();
			String piso = p.getDomicilio().getPiso();
			String dep = p.getDomicilio().getDepartamento();
			String tipoC = p.getTipoContacto().getNombre();
			
			Object[] fila = {nombre, tel, mail,cumple,loc,calle,alt,piso,dep,tipoC};
			this.getModelPersonas().addRow(fila);
		}
		
	}
}
