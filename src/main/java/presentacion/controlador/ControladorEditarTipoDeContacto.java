package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import modelo.Agenda;
import presentacion.vista.VentanaEditarTipoDeContacto;
import dto.TipoDeContactoDTO;

public class ControladorEditarTipoDeContacto implements ActionListener {

	// INSTANCIAS
	private VentanaEditarTipoDeContacto ventanaEditarTipoDeContacto;
	private Agenda agenda;
	private TipoDeContactoDTO tipoDeContactoModificado;
	// private ControladorVistaDeTiposDeContacto controlTipoContacto;

	// CONSTRUCTOR
	public ControladorEditarTipoDeContacto(TipoDeContactoDTO tipoDeContacto,
			Agenda agenda, VentanaEditarTipoDeContacto ventana,
			ControladorVistaDeTiposDeContacto control) {
		this.tipoDeContactoModificado = tipoDeContacto;
		this.agenda = agenda;
		ventanaEditarTipoDeContacto = ventana;
		this.ventanaEditarTipoDeContacto.getTextTipoDeContacto().setText(
				tipoDeContactoModificado.getNombre());
		this.ventanaEditarTipoDeContacto.getBtnEditarTipoDeContacto()
				.addActionListener(this);
	}

	// METODOS*****************
	public void initialize() {
		this.ventanaEditarTipoDeContacto.show();
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.ventanaEditarTipoDeContacto
				.getBtnEditarTipoDeContacto()) {

			this.tipoDeContactoModificado.setNombre(this.ventanaEditarTipoDeContacto
							.getTextTipoDeContacto().getText());
			this.agenda.actualizarTipoDeContacto(tipoDeContactoModificado);
			// ARREGLAR LA VISUALIZACION DE CAMBIO ANTES DE CERRAR LA VENTANA.
			//this.controlTipoContacto.initialize(); // SE ACTUALIZA VENTANA ANTES DE CERRAR.
			this.ventanaEditarTipoDeContacto.close();
		}
	}

}
