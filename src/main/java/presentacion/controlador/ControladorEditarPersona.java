package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import modelo.Agenda;
import presentacion.vista.VentanaEditarPersona;
import dto.DomicilioDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoDeContactoDTO;

//CLASE QUE IMPLEMENTA LAS ACCIONES CAPTURADAS DESDE LA VISTA ATRAVEZ DE METODOS EDITAR PERSONA.
public class ControladorEditarPersona implements ActionListener {

	// INSTANCIA DE VENTANA EDITAR PERSONA.
	private VentanaEditarPersona ventanaEditarPersona;

	// INSTANCIA QUE SE UTILIZA PARA REFRESCAR LA TABLA EDITAR PERSONA.
	private ControladorVistaGeneralPersona controladorVistaGeneralPersona;

	// INSTANCIAS AUX
	private PersonaDTO contactoEditado;
	private DomicilioDTO domicilioModificado;

	// CONSTRUCTOR
	public ControladorEditarPersona(VentanaEditarPersona ventana, ControladorVistaGeneralPersona control, PersonaDTO persona)
	{
		this.ventanaEditarPersona = ventana;
		this.contactoEditado = persona;
		this.controladorVistaGeneralPersona = control;

		cargarCBMlocalidades();
		cargarCMBtipoContacto();
		
		this.setearValoresVentanaEdicion();

		this.ventanaEditarPersona.getBtnEditarPersona().addActionListener(p->editarPersona(p));
		
		this.ventanaEditarPersona.mostrarVentana();
	}

	// METODO QUE CARGA LOS VALORES DE CONTACTO EN LA VISTA EDITAR.
	private void setearValoresVentanaEdicion() {
		ventanaEditarPersona.getTxtNombre().setText(this.contactoEditado.getNombre());
		ventanaEditarPersona.getTxtTelefono().setText(this.contactoEditado.getTelefono());
		domicilioModificado = Agenda.getAgenda().obtenerDomicilioPorId(this.contactoEditado.getDomicilio().getIdDomicilio());
		ventanaEditarPersona.getTextCalle().setText(domicilioModificado.getCalle());
		ventanaEditarPersona.getTextAltura().setText(domicilioModificado.getAltura());
		ventanaEditarPersona.getTextPiso().setText(domicilioModificado.getPiso());
		ventanaEditarPersona.getTextDepartamento().setText(domicilioModificado.getDepartamento());
		
		LocalidadDTO localidad = Agenda.getAgenda().obtenerLocalidadPorId(domicilioModificado.getLocalidad().getIdLocalidad());
		ventanaEditarPersona.getCmbSelecionLocalidad().setSelectedItem(localidad.getNombre());
		
		ventanaEditarPersona.getTxtEmail().setText(this.contactoEditado.getEmail());
		ventanaEditarPersona.getTxtFechaNacimiento().setText(this.contactoEditado.getFechaNacimiento());
		
		TipoDeContactoDTO tipoDeContacto = Agenda.getAgenda().obtenerTipoDeContactoPorId(contactoEditado.getTipoContacto().getIdTipoDeContacto());
		ventanaEditarPersona.getCmbTipoContacto().setSelectedItem(tipoDeContacto.getNombre());
	}

	// CARGA CMBOX TIPO CONTACTO CON LA INFORMACION DE LA BDD.
	private void cargarCMBtipoContacto() {
		List<TipoDeContactoDTO> tiposDeContactos = Agenda.getAgenda()
				.obtenerTiposDeContactos();
		for (TipoDeContactoDTO tipoDeContacto : tiposDeContactos) {
			ventanaEditarPersona.getCmbTipoContacto().addItem(
					tipoDeContacto.getNombre());
		}
	}

	// CARGA CMBOX LOCALIDADES CON LA INFORMACION DE LA BDD.
	private void cargarCBMlocalidades() {
		List<LocalidadDTO> localidades = Agenda.getAgenda()
				.obtenerLocalidades();
		for (LocalidadDTO localidad : localidades) {
			ventanaEditarPersona.getCmbSelecionLocalidad().addItem(
					localidad.getNombre());
		}
	}

	// GUARDA LOS TEXTOS DE LOS EVENTOS DE LA VENTANA EN OBJETO PERSONA
	private void editarPersona(ActionEvent p) {
		this.contactoEditado.setNombre(this.ventanaEditarPersona.getTxtNombre().getText());
		this.contactoEditado.setTelefono(this.ventanaEditarPersona.getTxtTelefono().getText());
		this.contactoEditado.setEmail(this.ventanaEditarPersona.getTxtEmail().getText());
		this.contactoEditado.setFechaNacimiento(this.ventanaEditarPersona.getTxtFechaNacimiento().getText());
		
		DomicilioDTO myDomicilio = this.contactoEditado.getDomicilio();
		myDomicilio.setCalle(this.ventanaEditarPersona.getTextCalle().getText());
		myDomicilio.setAltura(this.ventanaEditarPersona.getTextAltura().getText());
		myDomicilio.setPiso(this.ventanaEditarPersona.getTextPiso().getText());
		myDomicilio.setDepartamento(this.ventanaEditarPersona.getTextDepartamento().getText());
		/** MAS 1 PORQUE COMBO BOX LEE INDICE DESDE 1 Y LA POSISION ES DESDE 0 */
		myDomicilio.getLocalidad().setIdLocalidad(this.ventanaEditarPersona.getCmbSelecionLocalidad().getSelectedIndex()+1);
		/** MAS 1 PORQUE COMBO BOX LEE INDICE DESDE 1 Y LA POSISION ES DESDE 0 */
		this.contactoEditado.getTipoContacto().setIdTipoDeContacto(this.ventanaEditarPersona.getCmbTipoContacto().getSelectedIndex()+1);
		
		Agenda.getAgenda().actualizarDomicilio(myDomicilio);
		Agenda.getAgenda().actualizarPersona(this.contactoEditado);
		
		this.controladorVistaGeneralPersona.inicializar();
		this.ventanaEditarPersona.cerrar();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
