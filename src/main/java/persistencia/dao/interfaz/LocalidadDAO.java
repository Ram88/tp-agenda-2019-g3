package persistencia.dao.interfaz;

import java.util.List;

import dto.LocalidadDTO;

//CLASE INTERFAZ DE OBJETO LOCALIDAD DAO
public interface LocalidadDAO 
{

	public boolean insert(LocalidadDTO localidad);

	public boolean update(LocalidadDTO localidad);
		
	public boolean delete(LocalidadDTO localidad);

	public List<LocalidadDTO> readAll();

	public int traerporNombre(String nombre);
	
	public LocalidadDTO readForId(int idLocalidad);

}
