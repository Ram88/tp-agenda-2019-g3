package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.LocalidadDAO;
import dto.LocalidadDTO;

//CLASE QUE IMPLEMENTA LA INTERFAZ DE LOCALIDAD DAO PARA LA PERSISTENCIA EN BDD.
public class LocalidadDAOSQL implements LocalidadDAO
{
	private static final String insert = "INSERT INTO localidad(idLocalidad, nombre) VALUES(?, ?)";
	private static final String update = "UPDATE localidad Set nombre = ? Where idLocalidad = ?";
	private static final String delete = "DELETE FROM localidad WHERE idLocalidad = ? ";
	private static final String readall = "SELECT * FROM localidad";
	private static final String traerPorNombre = "SELECT idLocalidad FROM localidad Where nombre = ?";
	private static final String readForId = "SELECT * FROM localidad WHERE idLocalidad = ? ";

	public boolean insert(LocalidadDTO localidad) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setInt(1, localidad.getIdLocalidad());
			statement.setString(2, localidad.getNombre());
			if(statement.executeUpdate() > 0) //Si se ejecut� devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}

	public boolean update(LocalidadDTO localidad) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setString(1, localidad.getNombre());
			statement.setInt(2, localidad.getIdLocalidad());
			if(statement.executeUpdate() > 0) //Si se ejecut� devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}

	public boolean delete(LocalidadDTO localidad) {
		PreparedStatement statement;
		int chequeoUpdate = 0;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(delete);
			statement.setString(1, Integer.toString(localidad.getIdLocalidad()));
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) //Si se ejecutó devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public List<LocalidadDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<LocalidadDTO> localidades = new ArrayList<LocalidadDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				localidades.add(new LocalidadDTO(resultSet.getInt("idLocalidad"), resultSet.getString("nombre")));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return localidades;
	}
	
	public LocalidadDTO readForId(int idLocalidad) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readForId);
			statement.setInt(1, idLocalidad);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				return new LocalidadDTO(resultSet.getInt("idLocalidad"), resultSet.getString("nombre"));
			}	
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	public int traerporNombre(String nombre) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(traerPorNombre);
			statement.setString(1, nombre);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				return resultSet.getInt("idLocalidad");
			}
			
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		//Si no lo encuentra retorna 0?? ver como corregir
		return 0;
	}

}
