package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelo.Agenda;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.DomicilioDAO;
import dto.DomicilioDTO;
import dto.LocalidadDTO;

//CLASE QUE IMPLEMENTA LA INTERFAZ DE DOMICILIO DAO PARA LA PERSISTENCIA EN BDD.
public class DomicilioDAOSQL implements DomicilioDAO
{
	private static final String insert = "INSERT INTO domicilio(idDomicilio, calle, altura, piso, departamento, idLocalidad) VALUES(?, ?, ?, ?, ?, ?)";
	private static final String update = "UPDATE domicilio Set calle=?, altura = ?, piso = ?, departamento = ?, idLocalidad = ? Where idDomicilio = ?";
	private static final String delete = "DELETE FROM domicilio WHERE idDomicilio = ?";
	private static final String readall = "SELECT * FROM domicilio";
	private static final String readForId = "SELECT * FROM domicilio WHERE idDomicilio = ?";
	private static final String readForCalleAltura = "SELECT * FROM domicilio WHERE calle = ? and altura = ?";
	
	public boolean insert(DomicilioDTO domicilio) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setInt(1, domicilio.getIdDomicilio());
			statement.setString(2, domicilio.getCalle());
			statement.setString(3, domicilio.getAltura());
			statement.setString(4, domicilio.getPiso());
			statement.setString(5, domicilio.getDepartamento());
			statement.setInt(6, domicilio.getLocalidad().getIdLocalidad());
			if(statement.executeUpdate() > 0) //Si se ejecuta devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	public boolean update(DomicilioDTO domicilio) {

		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setString(1, domicilio.getCalle());
			statement.setString(2, domicilio.getAltura());
			statement.setString(3, domicilio.getPiso());
			statement.setString(4, domicilio.getDepartamento());
			statement.setInt(5, domicilio.getLocalidad().getIdLocalidad());
			statement.setInt(6, domicilio.getIdDomicilio());
			
			if(statement.executeUpdate() > 0) //Si se ejecuta devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}

	public boolean delete(DomicilioDTO domicilio_a_eliminar) {
		PreparedStatement statement;
		int chequeoUpdate = 0;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(delete);
			statement.setString(1, Integer.toString(domicilio_a_eliminar.getIdDomicilio()));
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) //Si se ejecuta devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}	
	
	public boolean deleteForId(int idDomicilio_a_eliminar) {
		PreparedStatement statement;
		int chequeoUpdate = 0;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(delete);
			statement.setString(1, Integer.toString(idDomicilio_a_eliminar));
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) //Si se ejecuta devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}	
	
	public List<DomicilioDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<DomicilioDTO> domicilios = new ArrayList<DomicilioDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				LocalidadDTO localidad = Agenda.getAgenda().obtenerLocalidadPorId(resultSet.getInt("idLocalidad"));
				domicilios.add(new DomicilioDTO(resultSet.getInt("idDomicilio"), resultSet.getString("calle"),
												resultSet.getString("altura"), resultSet.getString("piso"),
												resultSet.getString("departamento"), localidad));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return domicilios;
	}
	
	public int readForCalleAltura(String calle, String altura) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readForCalleAltura);
			statement.setString(1, calle);
	        statement.setString(2, altura);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				return resultSet.getInt("idDomicilio");
			}	
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return 0;//Si no lo encuentra retorna 0?? ver como solucionar.
	}

	public DomicilioDTO readForId(int idDomicilio) {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		DomicilioDTO domicilio = null; 
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readForId);
			statement.setInt(1, idDomicilio);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				LocalidadDTO localidad = Agenda.getAgenda().obtenerLocalidadPorId(resultSet.getInt("idLocalidad"));
				return domicilio = new DomicilioDTO(resultSet.getInt("idDomicilio"), resultSet.getString("calle"),
													resultSet.getString("altura"), resultSet.getString("piso"),
													resultSet.getString("departamento"), localidad);
			}	
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return domicilio;
	}

}
