package persistencia.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelo.Agenda;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PersonaDAO;
import dto.DomicilioDTO;
import dto.PersonaDTO;
import dto.TipoDeContactoDTO;

// CLASE QUE IMPLEMENTA LA INTERFAZ DE PERSONA DAO PARA LA PERSISTENCIA EN BDD.
public class PersonaDAOSQL implements PersonaDAO
{
	private static final String insert = "INSERT INTO personas(idPersona, nombre, telefono, idDomicilio, email, fechaCumple, idTipoDeContacto) VALUES(?, ?, ?, ?, ?, ?, ?)";
	private static final String update = "UPDATE personas Set nombre=?, telefono = ?, email = ?, fechaCumple = ?, idTipoDeContacto = ? Where idPersona = ?";
	private static final String delete = "DELETE FROM personas WHERE idPersona = ?";
	private static final String readall = "SELECT * FROM personas";
			
	public boolean insert(PersonaDTO persona)
	{
		Agenda.getAgenda().agregarDomicilio(persona.getDomicilio());
		int myDomicilio = Agenda.getAgenda().obtenerIddomicilio(persona.getDomicilio().getCalle(), persona.getDomicilio().getAltura());
		
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		
		try
		{
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, persona.getIdPersona());
			statement.setString(2, persona.getNombre());
			statement.setString(3, persona.getTelefono());
			statement.setInt(4, myDomicilio);
			statement.setString(5, persona.getEmail());
			statement.setString(6, persona.getFechaNacimiento());
			statement.setInt(7, persona.getTipoContacto().getIdTipoDeContacto());
			if(statement.executeUpdate() > 0)
			{
				return true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean delete(PersonaDTO persona_a_eliminar)
	{
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		try 
		{
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(persona_a_eliminar.getIdPersona()));
			if(statement.executeUpdate() > 0)
			{
				return true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public List<PersonaDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next())
			{
				DomicilioDTO myDomicilio = Agenda.getAgenda().obtenerDomicilioPorId(resultSet.getInt("idDomicilio"));
				TipoDeContactoDTO myContacto = Agenda.getAgenda().obtenerTipoDeContactoPorId(resultSet.getInt("idTipoDeContacto"));
				personas.add(new PersonaDTO(resultSet.getInt("idPersona"), 
											resultSet.getString("Nombre"), 
											resultSet.getString("Telefono"), 
											myDomicilio, 
											resultSet.getString("email"), 
											resultSet.getString("fechaCumple"), 
											myContacto));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}

	@Override
	public boolean update(PersonaDTO persona) 
	{
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setString(1, persona.getNombre());
			statement.setString(2, persona.getTelefono());
			statement.setString(3, persona.getEmail());
			statement.setString(4, persona.getFechaNacimiento());
			statement.setInt(5, persona.getTipoContacto().getIdTipoDeContacto());
			statement.setInt(6, persona.getIdPersona());
			if(statement.executeUpdate() > 0)
			{
				return true;
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}	
		return false;
	}
	
}
