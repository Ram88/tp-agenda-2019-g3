package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import modelo.Agenda;
import presentacion.vista.VentanaEditarLocalidad;
import dto.LocalidadDTO;

public class ControladorEditarLocalidad implements ActionListener {
	
	// INSTANCIAS
	private VentanaEditarLocalidad ventanaEditarLocalidad;
	private Agenda agenda;
	private LocalidadDTO localidadModificada;
	private ControladorVistaDeLocalidades controlLocalidad;

	//CONSTRUCTOR
	public ControladorEditarLocalidad(LocalidadDTO localidad, Agenda agenda,
			VentanaEditarLocalidad ventana,
			ControladorVistaDeLocalidades control) {
		this.localidadModificada = localidad;
		this.agenda = agenda;
		this.ventanaEditarLocalidad = ventana;
		this.controlLocalidad = control;
		this.ventanaEditarLocalidad.getTextLocalidad().setText(
				localidadModificada.getNombre());
		this.ventanaEditarLocalidad.getBtnEditarLocalidad().addActionListener(
				this);
	}

	// METODOS*****************
	public void initialize() {
		this.ventanaEditarLocalidad.show();
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.ventanaEditarLocalidad
				.getBtnEditarLocalidad()) {
			
			this.localidadModificada.setNombre(this.ventanaEditarLocalidad
					.getTextLocalidad().getText());
			this.agenda.actualizarLocalidad(localidadModificada);
			this.controlLocalidad.initialize(); // SE ACTUALIZA VENTANA ANTES DE CERRAR.
			this.ventanaEditarLocalidad.close();
		}
	}

}
