package presentacion.reportes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import dto.PersonaDTO;

public class OrdenamientoAgenda {

	// METODO QUE RECIBE UNA LISTA DE PERSONAS DTO Y LAS ORDENA 
	// POR LOCALIDAD Y LUEGO POR TIPO DE CONTACTO Y RETORNA UNA LISTA REPORTEPERSONMA.
	public static List<ReportePersona> OrdenamientoZona(List<PersonaDTO> personas) {
		
		List<ReportePersona> listPersona = new ArrayList<ReportePersona>();
		
		for(PersonaDTO person : personas)
		{
			ReportePersona Contacto = new ReportePersona (person.getDomicilio().getLocalidad().getNombre(),person.getNombre(),person.getTipoContacto().getNombre());
			listPersona.add(Contacto);
		}
		/** APLICO EL ORDENANMIENTO POR LOCALIDAD Y LUEGO POR TIPO DE CONTACTO*/
		Collections.sort(listPersona);
		
		return listPersona;
	}
	
	
}
