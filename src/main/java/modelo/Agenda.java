package modelo;

import java.util.List;

import dto.DomicilioDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoDeContactoDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.DomicilioDAO;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PersonaDAO;
import persistencia.dao.interfaz.TipoDeContactoDAO;
import persistencia.dao.mysql.DAOSQLFactory;

// CLASE LOGICA 
public class Agenda 
{
	
	public static Agenda instancia;
	private PersonaDAO persona;	
	private DomicilioDAO domicilio;
	private TipoDeContactoDAO tipoDeContacto;
	private LocalidadDAO localidad;
	
	/** METODO DE PATRON DE DISE�O */
	public static Agenda getAgenda()   
	{								
		if(instancia == null)
		{
			instancia = new Agenda(new DAOSQLFactory());
		}
		return instancia;
	}

	// CONSTRUCTOR
	public Agenda(DAOAbstractFactory metodo_persistencia)
	{
		this.persona = metodo_persistencia.createPersonaDAO();
		this.domicilio = metodo_persistencia.createDomicilioDAO();
		this.tipoDeContacto = metodo_persistencia.createTipoDeContactoDAO();
		this.localidad = metodo_persistencia.createLocalidadDAO();
	}
	
	/** ----METODOS DE AGREGAR--- */
	public void agregarPersona(PersonaDTO nuevaPersona)
	{
		this.persona.insert(nuevaPersona);
	}
	
	public void agregarDomicilio(DomicilioDTO nuevoDomicilio)
	{
		this.domicilio.insert(nuevoDomicilio);
	}
	
	public void agregarLocalidad(LocalidadDTO nuevaLocalidad)
	{
		this.localidad.insert(nuevaLocalidad);
	}
	
	public void agregarTipoDeContacto(TipoDeContactoDTO nuevoTipoDeContacto)
	{
		this.tipoDeContacto.insert(nuevoTipoDeContacto);
	}
	
	/** ----METODOS DE ACTUALIZAR--- */
	public void actualizarPersona(PersonaDTO persona_actualizada) 
	{
		this.persona.update(persona_actualizada);
	}
	
	public void actualizarDomicilio(DomicilioDTO domicilio_actualizado) 
	{
		this.domicilio.update(domicilio_actualizado);
	}
	
	public void actualizarLocalidad(LocalidadDTO localidad_actualizada) 
	{
		this.localidad.update(localidad_actualizada);
	}
	
	public void actualizarTipoDeContacto(TipoDeContactoDTO tipoDeContacto_actualizado) 
	{
		this.tipoDeContacto.update(tipoDeContacto_actualizado);
	}

	/** ----METODOS DE BORRAR--- */
	public void borrarPersona(PersonaDTO persona_a_eliminar) 
	{
		this.persona.delete(persona_a_eliminar);
	}
	
	public void borrarDomicilioi(DomicilioDTO domicilio_a_eliminar) 
	{
		this.domicilio.delete(domicilio_a_eliminar);
	}
	
	public void borrarLocalidad(LocalidadDTO localidad_a_eliminar) 
	{
		this.localidad.delete(localidad_a_eliminar);
	}
	
	public void borrarTipoDeContacto(TipoDeContactoDTO tipoDeContacto_a_eliminar) 
	{
		this.tipoDeContacto.delete(tipoDeContacto_a_eliminar);
	}
	
	/** ----METODOS DE OBTENCION DE DATOS--- */
	public List<PersonaDTO> obtenerPersonas()
	{
		return this.persona.readAll();		
	}
	
	public int obtenerIdporNombreDeTipoContacto(String nombre) {
		return this.tipoDeContacto.traerporNombre(nombre);
	}

	public int obtenerIdporNombreDeLocalidad(String nombre) {
		return this.localidad.traerporNombre(nombre);
	}
	
	public int obtenerIddomicilio(String calle, String altura) {
		return this.domicilio.readForCalleAltura(calle, altura);
	}
	
	public DomicilioDTO obtenerDomicilioPorId(int idDomicilio) {
		return this.domicilio.readForId(idDomicilio);
	}
	
	public LocalidadDTO obtenerLocalidadPorId(int idLocalidad) {
		return this.localidad.readForId(idLocalidad);
	}

	public TipoDeContactoDTO obtenerTipoDeContactoPorId(int idTipoDeContacto) {
		return this.tipoDeContacto.readForId(idTipoDeContacto);
	}
	
	public List<TipoDeContactoDTO> obtenerTiposDeContactos(){
		return this.tipoDeContacto.readAll();
	}

	public List<LocalidadDTO> obtenerLocalidades(){
		return this.localidad.readAll();
	}
	
}
