package presentacion.vista;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import java.awt.Color;

import javax.swing.JButton;

public class VentanaEditarTipoDeContacto {

	private JFrame frame;
	private JTextField textTipoDeContacto;
	private JButton btnEditarTipoDeContacto;

	//	CONSTRUCTOR
	public VentanaEditarTipoDeContacto() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Editar Tipo de Contacto");
		frame.setSize(300, 150);
		frame.getContentPane().setBackground(new Color(153, 204, 204));
	    frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textTipoDeContacto = new JTextField();
		textTipoDeContacto.setHorizontalAlignment(SwingConstants.CENTER);
		textTipoDeContacto.setBounds(20, 22, 241, 20);
		frame.getContentPane().add(textTipoDeContacto);
		textTipoDeContacto.setColumns(10);
		
		btnEditarTipoDeContacto = new JButton("Editar");
		btnEditarTipoDeContacto.setBounds(20, 65, 241, 30);
		btnEditarTipoDeContacto.setForeground(Color.BLACK);
		btnEditarTipoDeContacto.setBackground(new Color(0, 51, 255));
		frame.getContentPane().add(btnEditarTipoDeContacto);
	}

	public JTextField getTextTipoDeContacto() {
		return textTipoDeContacto;
	}

	public JButton getBtnEditarTipoDeContacto() {
		return btnEditarTipoDeContacto;
	}

	public void setTextTipoDeContacto(JTextField textTipoDeContacto) {
		this.textTipoDeContacto = textTipoDeContacto;
	}

	public void show() {
		frame.setVisible(true);
	}
	
	public void close()
	{
		frame.dispose();
	}

}
