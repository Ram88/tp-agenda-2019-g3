package presentacion.vista;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import java.awt.Color;

import javax.swing.JButton;

public class VentanaEditarLocalidad {

	private JFrame frame;
	private JTextField textLocalidad;
	private JButton btnEditarLocalidad;

	//	CONSTRUCTOR
	public VentanaEditarLocalidad() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Editar Localidad");
		frame.setSize(300, 150);
		frame.getContentPane().setBackground(new Color(153, 204, 204));
	    frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textLocalidad = new JTextField();
		textLocalidad.setHorizontalAlignment(SwingConstants.CENTER);
		textLocalidad.setBounds(20, 22, 241, 20);
		frame.getContentPane().add(textLocalidad);
		textLocalidad.setColumns(10);
		
		btnEditarLocalidad = new JButton("Editar");
		btnEditarLocalidad.setBounds(20, 65, 241, 30);
		btnEditarLocalidad.setForeground(Color.BLACK);
		btnEditarLocalidad.setBackground(new Color(0, 51, 255));
		frame.getContentPane().add(btnEditarLocalidad);
	}

	public JTextField getTextLocalidad() {
		return textLocalidad;
	}

	public void setTextLocalidad(JTextField textLocalidad) {
		this.textLocalidad = textLocalidad;
	}

	public JButton getBtnEditarLocalidad() {
		return btnEditarLocalidad;
	}

	public void show() {
		frame.setVisible(true);
	}
	
	public void close()
	{
		frame.dispose();
	}

}
