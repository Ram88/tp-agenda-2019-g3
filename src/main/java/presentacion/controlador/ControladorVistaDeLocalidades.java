package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import modelo.Agenda;
import presentacion.vista.VentanaAgregarLocalidad;
import presentacion.vista.VentanaEditarLocalidad;
import presentacion.vista.VistaDeLocalidades;
import dto.LocalidadDTO;

public class ControladorVistaDeLocalidades implements ActionListener
{
		private VistaDeLocalidades vista;
		private List<LocalidadDTO> localidades_en_tabla;
		ControladorAgregarLocalidad controladorAgregarLocalidad;
		ControladorEditarLocalidad controladorEditarLocalidad;
		private Agenda agenda;
		
		// CONSTRUCTOR
		public ControladorVistaDeLocalidades(Agenda agenda, VistaDeLocalidades vista)
		{
			this.vista = vista;
			this.vista.getBtnAgregar().addActionListener(this);
			this.vista.getBtnBorrar().addActionListener(this);
			this.vista.getBtnEditar().addActionListener(this);
			this.agenda = agenda;
			this.localidades_en_tabla = null;
			// MUESTRA LA VENTANA...
			this.vista.mostrarVentana();
		}
		
		public void initialize()
		{
			this.llenarTabla();
			this.vista.getFrame().repaint();
		}
		
		private void llenarTabla()
		{
			this.vista.getModelLocalidades().setRowCount(0); //Para vaciar la tabla
			this.vista.getModelLocalidades().setColumnCount(0);
			this.vista.getModelLocalidades().setColumnIdentifiers(this.vista.getNombreColumnas());
			
			this.localidades_en_tabla = agenda.obtenerLocalidades();
			for (int i = 0; i < this.localidades_en_tabla.size(); i ++)
			{
				Object[] fila = {this.localidades_en_tabla.get(i).getNombre()};
				this.vista.getModelLocalidades().addRow(fila);
			}			
		}
		
		public void actionPerformed(ActionEvent e) 
		{
			if(e.getSource() == this.vista.getBtnAgregar())
			{
				controladorAgregarLocalidad = new ControladorAgregarLocalidad(this.agenda, new VentanaAgregarLocalidad(), this);
				controladorAgregarLocalidad.initialize();
				//CORREGIR QUE ACTUALICE LA TABLA
				this.llenarTabla();
				///
				controladorAgregarLocalidad.initialize();
			}
			else if(e.getSource() == this.vista.getBtnEditar())
			{
				LocalidadDTO localidad = null;
				int[] filas_seleccionadas = this.vista.getTablaLocalidades().getSelectedRows();
				for (int fila:filas_seleccionadas)
				{
					localidad =  this.localidades_en_tabla.get(fila);	
				}
				controladorEditarLocalidad = new ControladorEditarLocalidad(localidad, this.agenda, new VentanaEditarLocalidad(),this);
				controladorEditarLocalidad.initialize();
				this.llenarTabla();
			}
			else if(e.getSource() == this.vista.getBtnBorrar())
			{
				int[] filas_seleccionadas = this.vista.getTablaLocalidades().getSelectedRows();
				for (int fila:filas_seleccionadas)
				{
					this.agenda.borrarLocalidad(this.localidades_en_tabla.get(fila));
				}			
				this.llenarTabla();
			}	
		}
		
}
