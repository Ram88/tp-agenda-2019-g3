package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import modelo.Agenda;
import presentacion.vista.VentanaAgregarPersona;
import dto.DomicilioDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.TipoDeContactoDTO;

//CLASE QUE IMPLEMENTA LAS ACCIONES CAPTURADAS DESDE LA VISTA AGREGAR PERSONA.
public class ControladorAgregarPersona implements ActionListener
{
		// INSTANCIA DE VENTANA AGREGAR PERSONA.
		private VentanaAgregarPersona ventanaAgregarPersona; 
		
		// INSTANCIA QUE SE UTILIZA PARA REFRESCAR LA TABLA AGREGAR PERSONA.
		private ControladorVistaGeneralPersona controladorVistaGeneralPersona;
		
		// CONSTRUCTOR
		public ControladorAgregarPersona(VentanaAgregarPersona vista, ControladorVistaGeneralPersona control)
		{
			ventanaAgregarPersona = vista;
			controladorVistaGeneralPersona = control;
			this.ventanaAgregarPersona.getBtnAgregarPersona().addActionListener(p->guardarPersona(p));
			
			cargarCBMlocalidades();
			cargarCMBtipoContacto();
			
			this.ventanaAgregarPersona.mostrarVentana();
		}
		
		// CARGA CMBOX TIPO CONTACTO CON LA INFORMACION DE LA BDD.
		private void cargarCMBtipoContacto() {
			List<TipoDeContactoDTO> tiposDeContactos = Agenda.getAgenda().obtenerTiposDeContactos();
			for(TipoDeContactoDTO tipoDeContacto : tiposDeContactos) {
				ventanaAgregarPersona.getCmbTipoContacto().addItem(tipoDeContacto.getNombre());
			}
		}

		// CARGA CMBOX LOCALIDADES CON LA INFORMACION DE LA BDD.
		private void cargarCBMlocalidades() {
			List<LocalidadDTO> localidades = Agenda.getAgenda().obtenerLocalidades();
			for(LocalidadDTO localidad : localidades) {
				ventanaAgregarPersona.getCmbSelecionLocalidad().addItem(localidad.getNombre());
			}
		}

		// GUARDA LOS TEXTOS DE LOS EVENTOS DE LA VENTANA EN OBJETO PERSONA
		private void guardarPersona(ActionEvent p) {
			
			int idPersona = 0;
			String nombre = this.ventanaAgregarPersona.getTxtNombre().getText();
			String telefono = this.ventanaAgregarPersona.getTxtTelefono().getText();
			String email = this.ventanaAgregarPersona.getTxtEmail().getText();
			String fechaNac = this.ventanaAgregarPersona.getTxtFechaNacimiento().getText();
			
			int idDom = 0;
			String calle = this.ventanaAgregarPersona.getTextCalle().getText();
			String altura = this.ventanaAgregarPersona.getTextAltura().getText();
			String piso = this.ventanaAgregarPersona.getTextPiso().getText();
			String dept = this.ventanaAgregarPersona.getTextDepartamento().getText();
			String nombreLoc = this.ventanaAgregarPersona.getCmbSelecionLocalidad().getSelectedItem().toString();
			String nombreTC = this.ventanaAgregarPersona.getCmbTipoContacto().getSelectedItem().toString();
			LocalidadDTO localidad = new LocalidadDTO(Agenda.getAgenda().obtenerIdporNombreDeLocalidad(nombreLoc),"");
			
			DomicilioDTO domicilio = new DomicilioDTO(idDom,calle,altura, piso, dept, localidad );
			
			TipoDeContactoDTO tipoContac = new TipoDeContactoDTO(Agenda.getAgenda().obtenerIdporNombreDeTipoContacto(nombreTC),"");
			
			PersonaDTO myPersona = new PersonaDTO(idPersona,nombre,telefono,domicilio, email, fechaNac,tipoContac);
			
			Agenda.getAgenda().agregarPersona(myPersona);
			
			this.ventanaAgregarPersona.cerrar();
			this.controladorVistaGeneralPersona.inicializar();
		}
		
		@Override
		public void actionPerformed(ActionEvent e) { }

		
}
