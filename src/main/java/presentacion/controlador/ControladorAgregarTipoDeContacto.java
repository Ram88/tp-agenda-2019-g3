package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import dto.TipoDeContactoDTO;
import modelo.Agenda;
import presentacion.vista.VentanaAgregarTipoDeContacto;

public class ControladorAgregarTipoDeContacto implements ActionListener
{
	//INSTANCIAS
	private VentanaAgregarTipoDeContacto ventanaAgregarTipoDeContacto;
	private Agenda agenda;
	// private ControladorVistaDeTiposDeContacto controlTipoContacto;
	
	// CONSTRUCTOR
	public ControladorAgregarTipoDeContacto(Agenda agenda, VentanaAgregarTipoDeContacto ventana, ControladorVistaDeTiposDeContacto control) {
		this.ventanaAgregarTipoDeContacto = ventana;
		this.agenda = agenda;
		this.ventanaAgregarTipoDeContacto.getBtnAgregarTipoDeContacto().addActionListener(this);
	}

	// METODOS**************************
	public void initialize()
	{
		this.ventanaAgregarTipoDeContacto.show();	
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == this.ventanaAgregarTipoDeContacto.getBtnAgregarTipoDeContacto()){
			TipoDeContactoDTO tipoDeContacto = new TipoDeContactoDTO(0,this.ventanaAgregarTipoDeContacto.getTextTipoDeContacto().getText());
			this.agenda.agregarTipoDeContacto(tipoDeContacto);
			
			this.ventanaAgregarTipoDeContacto.show();
			// ARREGLAR LA VISUALIZACION DE CAMBIO ANTES DE CERRAR LA VENTANA.
			//this.controlTipoContacto.initialize(); // SE ACTUALIZA VENTANA ANTES DE CERRAR.
			this.ventanaAgregarTipoDeContacto.close();		
		}
	}
	
}
