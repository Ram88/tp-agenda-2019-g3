package presentacion.vista;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.Color;

import javax.swing.JButton;

public class VistaDeLocalidades
{
	private JTable tablaLocalidades;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private DefaultTableModel modelLocalidades;
	private  String[] nombreColumnas = {"Localidades"};
	private JFrame frame;

	public VistaDeLocalidades() 
	{

		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize( 400, 400);
		frame.setTitle("Vista de localidades");
		frame.setLocationRelativeTo(null);
		frame.getContentPane().setLayout(null);
		
		JScrollPane spLocalidades = new JScrollPane();
		spLocalidades.setBounds(10, 10, 362, 280);
		this.frame.getContentPane().add(spLocalidades);
		
		modelLocalidades = new DefaultTableModel(null,nombreColumnas);
		tablaLocalidades = new JTable(modelLocalidades);

		tablaLocalidades.setBackground(Color.darkGray);
		tablaLocalidades.setForeground(Color.WHITE);
		
		tablaLocalidades.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaLocalidades.getColumnModel().getColumn(0).setResizable(false);
		spLocalidades.setViewportView(tablaLocalidades);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(23, 315, 86, 24);
		btnAgregar.setForeground(Color.BLACK);
		btnAgregar.setBackground(Color.GREEN);
		this.frame.getContentPane().add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(144, 315, 86, 24);
		btnEditar.setForeground(new Color(0, 0, 0));
		btnEditar.setBackground(Color.darkGray);
		this.frame.getContentPane().add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(265, 315, 86, 24);
		btnBorrar.setForeground(new Color(0, 0, 0));
		btnBorrar.setBackground(Color.RED);
		this.frame.getContentPane().add(btnBorrar);	
		
		frame.setVisible(true);
	}
	
	// METODO PARA MOSTRAR VENTANA
	public void mostrarVentana()
	{
		frame.setVisible(true);
	}
	
	////// METODOS GETTERS Y SETTERS *******************
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}

	public JButton getBtnEditar() 
	{
		return btnEditar;
	}
	
	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public DefaultTableModel getModelLocalidades() 
	{
		return modelLocalidades;
	}
	
	public JTable getTablaLocalidades()
	{
		return tablaLocalidades;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}

	public JFrame getFrame() {
		return frame;
	}
	
}
