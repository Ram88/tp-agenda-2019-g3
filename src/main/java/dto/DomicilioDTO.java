package dto;

public class DomicilioDTO {

	private int idDomicilio;	
	private String calle;
	private String altura;
	private String piso;
	private String departamento;
	private LocalidadDTO localidad;
	 
	public DomicilioDTO(int idDomicilio, String calle,String altura,String piso, String dept,LocalidadDTO localidad)
	{
		 this.idDomicilio= idDomicilio;
		 this.calle=calle;
		 this.altura=altura;
		 this.piso=piso;
		 this.departamento=dept;
		 this.localidad=localidad; 
	 }
	 
	public int getIdDomicilio() 
	{
		return idDomicilio;
	}
	
	public void setIdDomicilio(int idDomicilio) 
	{
		this.idDomicilio = idDomicilio;
	}
	 
	 public void setCalle (String calle)
	 {
		 this.calle=calle;
	 }
	 
	 public void setAltura (String altura)
	 {
		 this.altura=altura;
	 }
	 
	 public void setPiso (String piso)
	 {
		 this.piso=piso;
	 }
	 
	 public void setDepartamento (String dept)
	 {
		 this.departamento=dept;
	 }
	 
	 public void setLocalidad (LocalidadDTO localidad)
	 {
		 this.localidad=localidad;
	 }
	 
	 public String getCalle ()
	 {
		return calle;
	 }
	 
	 public String getAltura ()
	 {
		return altura;
	 }
	 
	 public String getPiso ()
	 {
		 return piso;
	 }
	 
	 public String getDepartamento ()
	 {
		 return departamento;
	 }
	 
	 public LocalidadDTO getLocalidad ()
	 {
		 return localidad;
	 }
	 
	}
