package presentacion.vista;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import java.awt.Color;
import javax.swing.JButton;

public class VistaDeTiposDeContacto
{
	private JTable tablaTiposDeContacto;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private DefaultTableModel modelTiposDeContacto;
	private  String[] nombreColumnas = {"Tipo de Contacto"};
	private JFrame frame;

	public VistaDeTiposDeContacto() 
	{

		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize( 400, 400);
		frame.setTitle("Vista de tipos de contacto");
		frame.setLocationRelativeTo(null);
		frame.getContentPane().setLayout(null);
		
		JScrollPane spTiposDeContacto = new JScrollPane();
		spTiposDeContacto.setBounds(10, 10, 362, 280);
		this.frame.add(spTiposDeContacto);
		
		modelTiposDeContacto = new DefaultTableModel(null,nombreColumnas);
		tablaTiposDeContacto = new JTable(modelTiposDeContacto);

		tablaTiposDeContacto.setBackground(Color.darkGray);
		tablaTiposDeContacto.setForeground(Color.WHITE);
		
		tablaTiposDeContacto.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaTiposDeContacto.getColumnModel().getColumn(0).setResizable(false);
		spTiposDeContacto.setViewportView(tablaTiposDeContacto);
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(23, 315, 86, 24);
		btnAgregar.setForeground(Color.BLACK);
		btnAgregar.setBackground(Color.GREEN);
		this.frame.getContentPane().add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(144, 315, 86, 24);
		btnEditar.setForeground(new Color(0, 0, 0));
		btnEditar.setBackground(Color.darkGray);
		this.frame.getContentPane().add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(265, 315, 86, 24);
		btnBorrar.setForeground(new Color(0, 0, 0));
		btnBorrar.setBackground(Color.RED);
		this.frame.getContentPane().add(btnBorrar);	
		
		frame.setVisible(true);
	}
	
	// METODO PARA MOSTRAR VENTANA
	public void mostrarVentana()
	{
		frame.setVisible(true);
	}
	
	////// METODOS GETTERS Y SETTERS *******************
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}

	public JButton getBtnEditar() 
	{
		return btnEditar;
	}
	
	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public DefaultTableModel getModelTipoDeContacto() 
	{
		return modelTiposDeContacto;
	}
	
	public JTable getTablaTipoDeContacto()
	{
		return tablaTiposDeContacto;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}
	
	public JFrame getFrame() {
		return frame;
	}
	
}
