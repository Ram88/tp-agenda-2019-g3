package persistencia.dao.interfaz;

// CLASE INTERFAZ PARA CREACION DE OBJETOS DTO QUE LUEGO PERSISTEN EN BDD.

public interface DAOAbstractFactory 
{
	public PersonaDAO createPersonaDAO();
	
	public DomicilioDAO createDomicilioDAO();

	public TipoDeContactoDAO createTipoDeContactoDAO();

	public LocalidadDAO createLocalidadDAO();
}
