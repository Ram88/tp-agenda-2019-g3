package presentacion.reportes;

// CLASE QUE IMPLEMENTA COMPARABLE.
public class ReportePersona implements Comparable<ReportePersona>{

	private String nameLocalidad;
	private String nameContacto;
	private String nameTipoContacto;

	public ReportePersona(String Localidad, String nombreApellido,String TipoDeContacto)
	{
		this.nameLocalidad = Localidad;
		this.nameContacto = nombreApellido;
		this.nameTipoContacto = TipoDeContacto;
	}
	
	public String getNameLocalidad() {
		return nameLocalidad;
	}

	public void setNameLocalidad(String nameLocalidad) {
		this.nameLocalidad = nameLocalidad;
	}

	public String getNameContacto() {
		return nameContacto;
	}

	public void setNameContacto(String nameContacto) {
		this.nameContacto = nameContacto;
	}

	public String getNameTipoContacto() {
		return nameTipoContacto;
	}

	public void setNameTipoContacto(String nameTipoContacto) {
		this.nameTipoContacto = nameTipoContacto;
	}

	// IMPLEMENTACION DE COMPARABLE.
	@Override
	public int compareTo(ReportePersona listaPersonas) {
		
		String Dato1 =new String(String.valueOf(this.getNameLocalidad()) + this.getNameTipoContacto());
		
		String Dato2=new String(String.valueOf(listaPersonas.getNameLocalidad())+listaPersonas.getNameTipoContacto());
		
		return Dato1.compareTo(Dato2);
	}
	
}
