package dto;

public class PersonaDTO 
{
	private int idPersona;
	private String nombre;
	private String telefono;
	private String email;
	private String fechaNacimiento;
	private DomicilioDTO domicilio;
	private TipoDeContactoDTO tipo_contacto;
	
	public PersonaDTO(int idPersona, String nombre, String telefono, DomicilioDTO domicilio, String email, String fechaNacimiento,  TipoDeContactoDTO tipo_contacto)
	{
		this.idPersona = idPersona;
		this.nombre = nombre;
		this.telefono = telefono;
		this.domicilio = domicilio;
		this.email = email;
		this.fechaNacimiento = fechaNacimiento;
		this.tipo_contacto = tipo_contacto;
	}
	
	public int getIdPersona() 
	{
		return this.idPersona;
	}

	public void setIdPersona(int idPersona) 
	{
		this.idPersona = idPersona;
	}

	public String getNombre() 
	{
		return this.nombre;
	}

	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}

	public String getTelefono() 
	{
		return this.telefono;
	}

	public void setTelefono(String telefono) 
	{
		this.telefono = telefono;
	}
	
	
	public DomicilioDTO getDomicilio() 
	{
		return domicilio;
	}
	
	public void setDomicilio(DomicilioDTO domicilio) 
	{
		this.domicilio=domicilio;
	}
	
	
	public String getEmail() 
	{
		return email;
	}
	
	public void setEmail(String email) 
	{
		this.email=email;
	}
	
	
	public String getFechaNacimiento() 
	{
		return fechaNacimiento;
	}
	
	public void setFechaNacimiento(String fnac) 
	{
		this.fechaNacimiento=fnac;
	}

	public TipoDeContactoDTO getTipoContacto() 
	{
		return tipo_contacto;
	}
	
	public void setTipoContacto(TipoDeContactoDTO tipo_contacto) 
	{
		this.tipo_contacto=tipo_contacto;
	}
	
}
