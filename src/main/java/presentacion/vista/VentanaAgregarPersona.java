package presentacion.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JSeparator;

// METODO VISUAL DE AGREGAR UNA PERSONA.
public class VentanaAgregarPersona
{
	private JFrame frame;
	private static final long serialVersionUID = 1L;
	private JTextField txtNombre;
	private JTextField txtTelefono;
	private JTextField txtEmail;
	private JTextField txtFechaNacimiento;
	private JTextField txtTipoContacto;
	private JComboBox<String> cmbTipoContacto;
	private JButton btnAgregarPersona;
	private JTextField textCalle;
	private JTextField textAltura;
	private JTextField textPiso;
	private JTextField textDepartamento;
	private JComboBox<String> cmbSelecionLocalidad;
	
	public VentanaAgregarPersona() 
	{
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize( 347, 467);
		frame.setTitle("Agregar Contacto");
		frame.setLocationRelativeTo(null);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNombreYApellido = new JLabel("Nombre y apellido");
		lblNombreYApellido.setBounds(10, 11, 113, 14);
		frame.add(lblNombreYApellido);
		
		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setBounds(10, 52, 113, 14);
		frame.add(lblTelfono);
			
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(10, 90, 113, 14);
		frame.add(lblEmail);

		JLabel lblFechaNacimiento= new JLabel("Fecha de Nacimiento");
		lblFechaNacimiento.setBounds(10, 134, 113, 14);
		frame.add(lblFechaNacimiento);
		
		JLabel lblTipoContacto= new JLabel("Tipo Contacto");
		lblTipoContacto.setBounds(10, 344, 113, 14);
		frame.add(lblTipoContacto);
		
		JLabel lblCalle = new JLabel("Calle");
		lblCalle.setBounds(10, 178, 46, 14);
		frame.add(lblCalle);
		
		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setBounds(10, 210, 46, 14);
		frame.add(lblAltura);
		
		JLabel lblPiso = new JLabel("Piso");
		lblPiso.setBounds(10, 248, 46, 14);
		frame.add(lblPiso);
		
		JLabel lblDep = new JLabel("Dep");
		lblDep.setBounds(153, 248, 46, 14);
		frame.add(lblDep);
		
		JLabel lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setBounds(10, 295, 113, 14);
		frame.add(lblLocalidad);
		
		
		txtNombre = new JTextField();
		txtNombre.setBounds(133, 8, 164, 20);
		frame.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtTelefono = new JTextField();
		txtTelefono.setBounds(133, 49, 164, 20);
		frame.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(133, 90, 164, 20);
		frame.add(txtEmail);
		txtEmail.setColumns(10);
		
		txtFechaNacimiento = new JTextField();
		txtFechaNacimiento.setBounds(133, 131, 164, 20);
		frame.add(txtFechaNacimiento);
		txtFechaNacimiento.setColumns(10);
	
		textCalle = new JTextField();
		textCalle.setColumns(10);
		textCalle.setBounds(133, 175, 164, 20);
		frame.add(textCalle);
		
		textAltura = new JTextField();
		textAltura.setColumns(10);
		textAltura.setBounds(133, 207, 164, 20);
		frame.add(textAltura);
		
		textPiso = new JTextField();
		textPiso.setColumns(10);
		textPiso.setBounds(59, 245, 63, 20);
		frame.add(textPiso);
		
		textDepartamento = new JTextField();
		textDepartamento.setColumns(10);
		textDepartamento.setBounds(201, 245, 63, 20);
		frame.add(textDepartamento);
		
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 160, 287, 2);
		frame.add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 331, 287, 2);
		frame.add(separator_1);
		

		cmbTipoContacto = new JComboBox<String>();
		cmbTipoContacto.setBounds(133, 341, 164, 20);
		frame.add(cmbTipoContacto);
		
		cmbSelecionLocalidad = new JComboBox<String>();
		cmbSelecionLocalidad.setBounds(133, 292, 164, 20);
		frame.add(cmbSelecionLocalidad);
		
		
		btnAgregarPersona = new JButton("Agregar");
		btnAgregarPersona.setBounds(10, 394, 311, 23);
		frame.add(btnAgregarPersona);
		
		frame.setVisible(false);
	}
	
	public void mostrarVentana()
	{
		frame.setVisible(true);
	}
	
	// ********** GETTER Y SETTERS DE COMBO BOX Y JTEXTFILE *************
	public JTextField getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(JTextField txtEmail) {
		this.txtEmail = txtEmail;
	}

	public JTextField getTxtFechaNacimiento() {
		return txtFechaNacimiento;
	}

	public void setTxtFechaNacimiento(JTextField txtFechaNacimiento) {
		this.txtFechaNacimiento = txtFechaNacimiento;
	}

	public JTextField getTxtTipoContacto() {
		return txtTipoContacto;
	}

	public void setTxtTipoContacto(JTextField txtTipoContacto) {
		this.txtTipoContacto = txtTipoContacto;
	}

	public JComboBox<String> getCmbTipoContacto() {
		return cmbTipoContacto;
	}

	public JTextField getTextCalle() {
		return textCalle;
	}

	public void setTextCalle(JTextField textCalle) {
		this.textCalle = textCalle;
	}

	public JTextField getTextAltura() {
		return textAltura;
	}

	public void setTextAltura(JTextField textAltura) {
		this.textAltura = textAltura;
	}

	public JTextField getTextPiso() {
		return textPiso;
	}

	public void setTextPiso(JTextField textPiso) {
		this.textPiso = textPiso;
	}

	public JTextField getTextDepartamento() {
		return textDepartamento;
	}

	public void setTextDepartamento(JTextField textDepartamento) {
		this.textDepartamento = textDepartamento;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setTxtNombre(JTextField txtNombre) {
		this.txtNombre = txtNombre;
	}

	public void setTxtTelefono(JTextField txtTelefono) {
		this.txtTelefono = txtTelefono;
	}
	
	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}

	public JTextField getTxtTelefono() 
	{
		return txtTelefono;
	}

	public void setBtnAgregarPersona(JButton btnAgregarPersona) {
		this.btnAgregarPersona = btnAgregarPersona;
	}

	public JComboBox<String> getCmbSelecionLocalidad() {
		return cmbSelecionLocalidad;
	}
	
	public JButton getBtnAgregarPersona() 
	{
		return btnAgregarPersona;
	}

	public void cerrar()
	{
		frame.dispose();
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}

