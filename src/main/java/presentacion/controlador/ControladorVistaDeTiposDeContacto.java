package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import modelo.Agenda;
import presentacion.vista.VentanaAgregarTipoDeContacto;
import presentacion.vista.VentanaEditarTipoDeContacto;
import presentacion.vista.VistaDeTiposDeContacto;
import dto.TipoDeContactoDTO;

public class ControladorVistaDeTiposDeContacto implements ActionListener
{
		private VistaDeTiposDeContacto vista;
		private List<TipoDeContactoDTO> tiposDeContacto_en_tabla;
		ControladorAgregarTipoDeContacto controladorAgregarTipoDeContacto;
		ControladorEditarTipoDeContacto controladorEditarTipoDeContacto;
		private Agenda agenda;
		
		public ControladorVistaDeTiposDeContacto(Agenda agenda, VistaDeTiposDeContacto vista)
		{
			this.vista = vista;
			this.vista.getBtnAgregar().addActionListener(this);
			this.vista.getBtnBorrar().addActionListener(this);
			this.vista.getBtnEditar().addActionListener(this);
			this.agenda = agenda;
			this.tiposDeContacto_en_tabla = null;
			// MUESTRA LA VENTANA...
			this.vista.mostrarVentana();
		}
		
		public void initialize()
		{
			this.llenarTabla();
			this.vista.getFrame().repaint();
		}
		
		private void llenarTabla()
		{
			this.vista.getModelTipoDeContacto().setRowCount(0); //Para vaciar la tabla
			this.vista.getModelTipoDeContacto().setColumnCount(0);
			this.vista.getModelTipoDeContacto().setColumnIdentifiers(this.vista.getNombreColumnas());
			
			this.tiposDeContacto_en_tabla = this.agenda.obtenerTiposDeContactos();
			for (int i = 0; i < this.tiposDeContacto_en_tabla.size(); i ++)
			{
				Object[] fila = {this.tiposDeContacto_en_tabla.get(i).getNombre()};
				this.vista.getModelTipoDeContacto().addRow(fila);
			}			
		}
		
		public void actionPerformed(ActionEvent e) 
		{
			if(e.getSource() == this.vista.getBtnAgregar())
			{
				controladorAgregarTipoDeContacto = new ControladorAgregarTipoDeContacto(this.agenda, new VentanaAgregarTipoDeContacto(), this);
				controladorAgregarTipoDeContacto.initialize();
				//CORREGIR QUE ACTUALICE LA TABLA
				this.llenarTabla();
				this.controladorAgregarTipoDeContacto.initialize();////
			}
			else if(e.getSource() == this.vista.getBtnEditar())
			{
				TipoDeContactoDTO tipoDeContacto = null;
				int[] filas_seleccionadas = this.vista.getTablaTipoDeContacto().getSelectedRows();
				for (int fila:filas_seleccionadas)
				{
					tipoDeContacto =  this.tiposDeContacto_en_tabla.get(fila);	
				}
				controladorEditarTipoDeContacto= new ControladorEditarTipoDeContacto(tipoDeContacto, this.agenda, new VentanaEditarTipoDeContacto(), this);
				controladorEditarTipoDeContacto.initialize();
			}
			else if(e.getSource() == this.vista.getBtnBorrar())
			{
				int[] filas_seleccionadas = this.vista.getTablaTipoDeContacto().getSelectedRows();
				for (int fila:filas_seleccionadas)
				{
					this.agenda.borrarTipoDeContacto(this.tiposDeContacto_en_tabla.get(fila));
				}			
				this.llenarTabla();
			}
			
		}

}
