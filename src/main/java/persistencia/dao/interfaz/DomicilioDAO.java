package persistencia.dao.interfaz;

import java.util.List;

import dto.DomicilioDTO;

//CLASE INTERFAZ DE OBJETO DOMICILIO DAO
public interface DomicilioDAO 
{
	public boolean insert(DomicilioDTO domicilio);

	public boolean update(DomicilioDTO domicilio);
	
	public boolean delete(DomicilioDTO domicilio_a_eliminar);

	public boolean deleteForId(int IdDomicilio_a_eliminar);
	
	public List<DomicilioDTO> readAll();

	public int readForCalleAltura(String calle, String altura);
	
	public DomicilioDTO readForId(int idDomicilio);
	
}
