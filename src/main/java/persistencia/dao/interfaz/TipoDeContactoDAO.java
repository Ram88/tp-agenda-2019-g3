package persistencia.dao.interfaz;

import java.util.List;

import dto.TipoDeContactoDTO;

//CLASE INTERFAZ DE OBJETO TIPO DE CONTACTO DAO
public interface TipoDeContactoDAO 
{

	public boolean insert(TipoDeContactoDTO tipoDeContacto);

	public boolean update(TipoDeContactoDTO tipoDeContacto);
	
	public boolean delete(TipoDeContactoDTO tipoDeContacto);
	
	public List<TipoDeContactoDTO> readAll();

	public int traerporNombre(String nombre);
	
	public TipoDeContactoDTO readForId(int idTipoDeContacto);
	
}
