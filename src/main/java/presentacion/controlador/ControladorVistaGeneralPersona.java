package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import modelo.Agenda;
import presentacion.reportes.ReporteAgenda;
import presentacion.reportes.OrdenamientoAgenda;
import presentacion.vista.VentanaAgregarPersona;
import presentacion.vista.VentanaEditarPersona;
import presentacion.vista.VistaDeLocalidades;////////////////
import presentacion.vista.VistaDeTiposDeContacto;
import presentacion.vista.VistaGeneralPersona;
import dto.PersonaDTO;

// CLASE QUE IMPLEMENTA LAS ACCIONES CAPTURADAS DESDE LA VISTA GENERAL.
public class ControladorVistaGeneralPersona implements ActionListener {
	
	private VistaGeneralPersona vista;
	private List<PersonaDTO> personasEnTabla;
	private Agenda agenda;
	private PersonaDTO personaEditada;
	ControladorVistaDeLocalidades controladorVistaDeLocalidades;
	ControladorVistaDeTiposDeContacto controladorVistaDeTiposDeContacto;

	// CONSTRUTOR
	public ControladorVistaGeneralPersona(VistaGeneralPersona vista,
			Agenda agenda) {
		this.vista = vista;
		this.vista.getBtnAgregar().addActionListener(a -> ventanaAgregarPersona(a));
		this.vista.getBtnEditar().addActionListener(e -> editarPersona(e));
		this.vista.getBtnBorrar().addActionListener(s -> borrarPersona(s));
		this.vista.getBtnReporte().addActionListener(r -> mostrarReporte(r));
		this.vista.getBtnAbmLocalidad().addActionListener(l -> localidadABM (l));
		this.vista.getBtnAbmTipoCon().addActionListener(c -> tipoContactoABM (c));////////////
		
		this.agenda = agenda;
	}

	// **** METODOS ****
	private void ventanaAgregarPersona(ActionEvent a) {
		VentanaAgregarPersona vista = new VentanaAgregarPersona();
		@SuppressWarnings("unused")
		ControladorAgregarPersona controlador = new ControladorAgregarPersona(
				vista, this);
	}

	private void editarPersona(ActionEvent a) {
		this.personasEnTabla = agenda.obtenerPersonas();
		// PROBLEMA CUANDO NINGUNO ESTA SELECCIONADO.
		this.personaEditada = this.personasEnTabla.get(this.vista.getTablaPersonas().getSelectedRow());
		@SuppressWarnings("unused")
		ControladorEditarPersona controladorVista = new ControladorEditarPersona(new VentanaEditarPersona(), this, personaEditada);
	}

	private void mostrarReporte(ActionEvent r) {
		ReporteAgenda reporte = new ReporteAgenda(OrdenamientoAgenda.OrdenamientoZona(agenda.obtenerPersonas()));
		reporte.mostrar();
	}

	public void borrarPersona(ActionEvent s) {
		int[] filasSeleccionadas = this.vista.getTablaPersonas()
				.getSelectedRows();
		for (int fila : filasSeleccionadas) {
			this.agenda.borrarDomicilioi(this.personasEnTabla.get(fila).getDomicilio());
			this.agenda.borrarPersona(this.personasEnTabla.get(fila));
		}
		this.refrescarTabla();
	}

	//***** METODOS ABM ****
	public void localidadABM (ActionEvent l)
	{
		controladorVistaDeLocalidades = new ControladorVistaDeLocalidades(this.agenda, new VistaDeLocalidades());
		controladorVistaDeLocalidades.initialize();
	}
	
	public void tipoContactoABM (ActionEvent l)
	{
		controladorVistaDeTiposDeContacto = new ControladorVistaDeTiposDeContacto(this.agenda, new VistaDeTiposDeContacto());
		controladorVistaDeTiposDeContacto.initialize();
	}
	
	public void inicializar() {
		this.refrescarTabla();
		this.vista.show();
	}

	private void refrescarTabla() {
		this.personasEnTabla = agenda.obtenerPersonas();
		this.vista.llenarTabla(this.personasEnTabla);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}

}
